package br.com.itau;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scannner = new Scanner(System.in);
        Entrada entrada = new Entrada();
        Calculadora calculadora = new Calculadora();

        System.out.print("Digite um valor: ");
        entrada.setValorDeposito(scannner.nextDouble());

        System.out.print("Digite a quantidade de meses: ");
        entrada.setQuantidadeMeses(scannner.nextInt());

        calculadora.calcular(entrada);

    }
}
