package br.com.itau;

public class Entrada {

    private double valorDeposito;
    private int quantidadeMeses;

    public double getValorDepositado() {
        return valorDeposito;
    }

    public void setValorDeposito(double valorDeposito) {
        this.valorDeposito = valorDeposito;
    }

    public double getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public void atualizarValorDepositado(double valor){
        this.setValorDeposito(valor);
    }

}
