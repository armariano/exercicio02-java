package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Calculadora {

    private double juros;
    private double rendimento;
    private Resultado resultado;
    private List<Resultado> resultados;

    public List<Resultado> calcular(Entrada entrada){

        resultados = new ArrayList<Resultado>();
        for (int i =1; i <= entrada.getQuantidadeMeses(); i++){
            resultado = new Resultado();
            resultado.setMes(i);

            juros = (entrada.getValorDepositado() *0.70) / 100;
            resultado.setValorRendimento(juros + entrada.getValorDepositado());

            resultados.add(resultado);

            entrada.atualizarValorDepositado(resultado.getValorRendimento());

            Impressao.imprimir("Mês: " + resultado.getMes() + " - Valor do Rendimento (R$): " + resultado.getValorRendimento());
        }

        return resultados;
    }

}
