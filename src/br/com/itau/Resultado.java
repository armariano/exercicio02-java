package br.com.itau;

public class Resultado {

    private int mes;
    private double valorRendimento;

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getMes() {
        return mes;
    }

    public void setValorRendimento(double valorRendimento) {
        this.valorRendimento = valorRendimento;
    }

    public double getValorRendimento() {
        return valorRendimento;
    }

}
